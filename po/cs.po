# Czech translations for malcontent package.
# Copyright (C) 2020 THE malcontent'S COPYRIGHT HOLDER
# This file is distributed under the same license as the malcontent package.
# Automatically generated, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: malcontent\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/pwithnall/malcontent/"
"issues\n"
"POT-Creation-Date: 2024-03-16 03:25+0000\n"
"PO-Revision-Date: 2024-03-17 00:04+0100\n"
"Last-Translator: Daniel Rusek <mail@asciiwolf.com>\n"
"Language-Team: none\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Poedit 3.4.2\n"

#: accounts-service/com.endlessm.ParentalControls.policy.in:4
msgid "Change your own app filter"
msgstr "Měnit filtr vlastních aplikací"

#: accounts-service/com.endlessm.ParentalControls.policy.in:5
msgid "Authentication is required to change your app filter."
msgstr "Pro změnu vlastního filtru aplikací je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:14
msgid "Read your own app filter"
msgstr "Číst filtr vlastních aplikací"

#: accounts-service/com.endlessm.ParentalControls.policy.in:15
msgid "Authentication is required to read your app filter."
msgstr "Pro čtení vlastního filtru aplikací je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:24
msgid "Change another user’s app filter"
msgstr "Měnit filtr aplikací jiného uživatele"

#: accounts-service/com.endlessm.ParentalControls.policy.in:25
msgid "Authentication is required to change another user’s app filter."
msgstr "Pro změnu filtru aplikací jiného uživatele je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:34
msgid "Read another user’s app filter"
msgstr "Číst filtr aplikací jiného uživatele"

#: accounts-service/com.endlessm.ParentalControls.policy.in:35
msgid "Authentication is required to read another user’s app filter."
msgstr "Pro čtení filtru aplikací jiného uživatele je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:44
msgid "Change your own session limits"
msgstr "Měnit limity vlastního sezení"

#: accounts-service/com.endlessm.ParentalControls.policy.in:45
msgid "Authentication is required to change your session limits."
msgstr "Pro změnu limitů vlastního sezení je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:54
msgid "Read your own session limits"
msgstr "Číst limity vlastního sezení"

#: accounts-service/com.endlessm.ParentalControls.policy.in:55
msgid "Authentication is required to read your session limits."
msgstr "Pro čtení limitů vlastního sezení je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:64
msgid "Change another user’s session limits"
msgstr "Měnit limity sezení jiného uživatele"

#: accounts-service/com.endlessm.ParentalControls.policy.in:65
msgid "Authentication is required to change another user’s session limits."
msgstr "Pro změnu limitů sezení jiného uživatele je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:74
msgid "Read another user’s session limits"
msgstr "Číst limity sezení jiného uživatele"

#: accounts-service/com.endlessm.ParentalControls.policy.in:75
msgid "Authentication is required to read another user’s session limits."
msgstr "Pro čtení limitů sezení jiného uživatele je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:84
msgid "Change your own account info"
msgstr "Měnit informace o vlastním účtu"

#: accounts-service/com.endlessm.ParentalControls.policy.in:85
msgid "Authentication is required to change your account info."
msgstr "Pro změnu informací o vlastním účtu je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:94
msgid "Read your own account info"
msgstr "Číst informace o vlastním účtu"

#: accounts-service/com.endlessm.ParentalControls.policy.in:95
msgid "Authentication is required to read your account info."
msgstr "Pro čtení informací o vlastním účtu je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:104
msgid "Change another user’s account info"
msgstr "Měnit informace o účtu jiného uživatele"

#: accounts-service/com.endlessm.ParentalControls.policy.in:105
msgid "Authentication is required to change another user’s account info."
msgstr "Pro změnu informací o účtu jiného uživatele je vyžadováno ověření."

#: accounts-service/com.endlessm.ParentalControls.policy.in:114
msgid "Read another user’s account info"
msgstr "Číst informace o účtu jiného uživatele"

#: accounts-service/com.endlessm.ParentalControls.policy.in:115
msgid "Authentication is required to read another user’s account info."
msgstr "Pro čtení informací o účtu jiného uživatele je vyžadováno ověření."

#: libmalcontent/app-filter.c:696
#, c-format
msgid "App filter for user %u was in an unrecognized format"
msgstr "Filtr aplikací pro uživatele %u byl v nerozpoznaném formátu"

#: libmalcontent/app-filter.c:727
#, c-format
msgid "OARS filter for user %u has an unrecognized kind ‘%s’"
msgstr "OARS filtr pro uživatele %u má nerozpoznaný typ ‘%s’"

#: libmalcontent/manager.c:283 libmalcontent/manager.c:420
#: libmalcontent/manager.c:803
#, c-format
msgid "Not allowed to query parental controls data for user %u"
msgstr ""
"Není povoleno dotazovat se na data rodičovské kontroly pro uživatele %u"

#: libmalcontent/manager.c:288
#, c-format
msgid "User %u does not exist"
msgstr "Uživatel %u neexistuje"

#: libmalcontent/manager.c:296
msgid "System accounts service not available"
msgstr "Služba systémových účtů není dostupná"

#: libmalcontent/manager.c:402
msgid "App filtering is globally disabled"
msgstr "Filtrování aplikací je globálně zakázáno"

#: libmalcontent/manager.c:785
msgid "Session limits are globally disabled"
msgstr "Limity sezení jsou globálně zakázány"

#: libmalcontent/session-limits.c:306
#, c-format
msgid "Session limit for user %u was in an unrecognized format"
msgstr "Limit sezení pro uživatele %u byl v nerozpoznaném formátu"

#: libmalcontent/session-limits.c:328
#, c-format
msgid "Session limit for user %u has an unrecognized type ‘%u’"
msgstr "Limit sezení pro uživatele %u má nerozpoznaný typ ‘%u’"

#: libmalcontent/session-limits.c:346
#, c-format
msgid "Session limit for user %u has invalid daily schedule %u–%u"
msgstr "Limit sezení pro uživatele %u má neplatný denní rozvrh %u–%u"

#. Translators: the placeholder is a user’s full name
#: libmalcontent-ui/restrict-applications-dialog.c:256
#, c-format
msgid "Restrict %s from using the following installed applications."
msgstr "Omezit uživateli %s použití následujících nainstalovaných aplikací."

#: libmalcontent-ui/restrict-applications-dialog.ui:6
msgid "Restrict Applications"
msgstr "Omezit aplikace"

#: libmalcontent-ui/restrict-applications-dialog.ui:22
msgid "Search for applications…"
msgstr "Hledat aplikace…"

#: libmalcontent-ui/restrict-applications-selector.ui:13
msgid "No applications found to restrict."
msgstr "Nenalezeny žádné aplikace k omezení."

#. Translators: this is the full name for an unknown user account.
#: libmalcontent-ui/user-controls.c:198 libmalcontent-ui/user-controls.c:209
msgid "unknown"
msgstr "neznámé"

#: libmalcontent-ui/user-controls.c:306 libmalcontent-ui/user-controls.c:394
#: libmalcontent-ui/user-controls.c:647
msgid "Unrestricted"
msgstr "Bez omezení"

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:476
#, c-format
msgid ""
"Prevents %s from running web browsers. Limited web content may still be "
"available in other applications."
msgstr ""
"Zabraňuje uživateli %s ve spouštění webových prohlížečů. Omezený webový "
"obsah může být stále dostupný v jiných aplikacích."

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:481
#, c-format
msgid "Prevents specified applications from being used by %s."
msgstr "Zabraňuje v používání určených aplikací uživatelem %s."

#. Translators: The placeholder is a user’s display name.
#: libmalcontent-ui/user-controls.c:486
#, c-format
msgid "Prevents %s from installing applications."
msgstr "Zabraňuje uživateli %s v instalaci aplikací."

#: libmalcontent-ui/user-controls.ui:25
msgid "Application Usage Restrictions"
msgstr "Omezení použití aplikací"

#: libmalcontent-ui/user-controls.ui:28
msgid "Restrict _Web Browsers"
msgstr "Omezit _webové prohlížeče"

#: libmalcontent-ui/user-controls.ui:51
msgid "_Restrict Applications"
msgstr "Omezit _aplikace"

#: libmalcontent-ui/user-controls.ui:73
msgid "Software Installation Restrictions"
msgstr "Omezení instalace softwaru"

#: libmalcontent-ui/user-controls.ui:77
msgid "Restrict Application _Installation"
msgstr "Omezit _instalaci aplikací"

#: libmalcontent-ui/user-controls.ui:99
msgid "Application _Suitability"
msgstr "Vhodno_st aplikací"

#: libmalcontent-ui/user-controls.ui:102
msgid ""
"Restricts the browsing or installation of applications unsuitable for this "
"age or younger."
msgstr ""
"Omezí prohlížení nebo instalaci aplikací nevhodných pro tento věk nebo "
"mladší."

#. Translators: This documents the --user command line option to malcontent-control:
#: malcontent-control/application.c:102
msgid "User to select in the UI"
msgstr "Uživatel ke zvolení v UI"

#. Translators: This is a placeholder for a command line argument value:
#: malcontent-control/application.c:104
msgid "USERNAME"
msgstr "UŽIVATEL"

#: malcontent-control/application.c:116
msgid "— view and edit parental controls"
msgstr "— zobrazit a upravit rodičovskou kontrolu"

#. Translators: This is the title of the main window
#. Translators: the name of the application as it appears in a software center
#: malcontent-control/application.c:123 malcontent-control/main.ui:17
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:9
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:3
msgid "Parental Controls"
msgstr "Rodičovská kontrola"

#: malcontent-control/application.c:309
msgid "Copyright © 2019, 2020 Endless Mobile, Inc."
msgstr "Copyright © 2019, 2020 Endless Mobile, Inc."

#. Translators: this should be "translated" to the
#. names of people who have translated Malcontent into
#. this language, one per line.
#: malcontent-control/application.c:314
msgid "translator-credits"
msgstr "Daniel Rusek <mail@asciiwolf.com>"

#. Translators: "Malcontent" is the brand name of this
#. project, so should not be translated.
#: malcontent-control/application.c:320
msgid "Malcontent Website"
msgstr "Webové stránky"

#: malcontent-control/application.c:340
msgid "The help contents could not be displayed"
msgstr "Obsah nápovědy nemohl být zobrazen"

#: malcontent-control/application.c:389
msgid "Failed to load user data from the system"
msgstr "Selhalo načtení uživatelských dat ze systému"

#: malcontent-control/application.c:391
msgid "Please make sure that the AccountsService is installed and enabled."
msgstr "Ověřte prosím, že je AccountsService nainstalováno a povoleno."

#. Translators: Replace the link to commonsensemedia.org with some
#. * localised guidance for parents/carers on how to set restrictions on
#. * their child/caree in a responsible way which is in keeping with the
#. * best practice and culture of the region. If no suitable localised
#. * guidance exists, and if the default commonsensemedia.org link is not
#. * suitable, please file an issue against malcontent so we can discuss
#. * further!
#. * https://gitlab.freedesktop.org/pwithnall/malcontent/-/issues/new
#.
#: malcontent-control/application.c:424
#, c-format
msgid ""
"It’s recommended that restrictions are set as part of an ongoing "
"conversation with %s. <a href='https://www.commonsensemedia.org/privacy-and-"
"internet-safety'>Read guidance</a> on what to consider."
msgstr ""
"Je doporučeno, aby byla omezení nastavena jako součást probíhající "
"konverzace s %s. <a href='https://www.jaknainternet.cz/page/1201/ochrana-"
"deti-na-internetu/'>Přečtěte si návod</a> na to, co je třeba vzít v úvahu."

#: malcontent-control/carousel.ui:38
msgid "Previous Page"
msgstr "Předchozí stránka"

#: malcontent-control/carousel.ui:57
msgid "Next Page"
msgstr "Následující stránka"

#: malcontent-control/main.ui:87
msgid "Permission Required"
msgstr "Vyžadováno oprávnění"

#: malcontent-control/main.ui:88
msgid ""
"Permission is required to view and change user parental controls settings."
msgstr ""
"Pro zobrazení a změnu rodičovské kontroly uživatele je nutné oprávnění."

#: malcontent-control/main.ui:110
msgid "No Standard User Accounts"
msgstr "Žádný běžný uživatelský účet"

#: malcontent-control/main.ui:111
msgid ""
"Parental controls can only be applied to standard user\n"
"accounts. These can be created in the user settings."
msgstr ""
"Rodičovská kontrola může být aplikována pouze na standardní uživatelské\n"
"účty. Ty mohou být vytvořeny v nastavení uživatelů."

#: malcontent-control/main.ui:115
msgid "_User Settings"
msgstr "Nastavení _uživatelů"

#: malcontent-control/main.ui:138
msgid "Loading…"
msgstr "Načítá se…"

#: malcontent-control/main.ui:171
msgid "_Help"
msgstr "_Nápověda"

#: malcontent-control/main.ui:175
msgid "_About Parental Controls"
msgstr "O _aplikaci Rodičovská kontrola"

#. Translators: the brief summary of the application as it appears in a software center.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:12
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:4
msgid "Set parental controls and monitor usage by users"
msgstr "Nastavte rodičovskou kontrolu a monitorujte používání uživateli"

#. Translators: These are the application description paragraphs in the AppData file.
#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:16
msgid ""
"Manage users’ parental controls restrictions, controlling how long they can "
"use the computer for, what software they can install, and what installed "
"software they can run."
msgstr ""
"Spravujte omezení rodičovské kontroly uživatelů, určující jak dlouho mohou "
"používat počítač, jaký software mohou instalovat a jaký software mohou "
"spouštět."

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:25
msgid "Main window"
msgstr "Hlavní okno"

#: malcontent-control/org.freedesktop.MalcontentControl.appdata.xml.in:40
msgid "The GNOME Project"
msgstr "Projekt GNOME"

#. Translators: Search terms to find this application. Do NOT translate or localise the semicolons! The list MUST also end with a semicolon!
#: malcontent-control/org.freedesktop.MalcontentControl.desktop.in:14
msgid ""
"parental controls;screen time;app restrictions;web browser restrictions;oars;"
"usage;usage limit;kid;child;"
msgstr ""
"rodičovská kontrola;čas u obrazovky;omezení aplikací;omezení webového "
"prohlížeče;oars;použití;omezení použití;dítě;děti;"

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:9
msgid "Manage parental controls"
msgstr "Spravovat rodičovskou kontrolu"

#: malcontent-control/org.freedesktop.MalcontentControl.policy.in:10
msgid "Authentication is required to read and change user parental controls"
msgstr ""
"Pro zobrazení a změnu rodičovské kontroly uživatele je vyžadováno ověření"

#: malcontent-control/user-selector.c:426
msgid "Your account"
msgstr "Váš účet"

#. Always allow root, to avoid a situation where this PAM module prevents
#. * all users logging in with no way of recovery.
#: pam/pam_malcontent.c:142 pam/pam_malcontent.c:188
#, c-format
msgid "User ‘%s’ has no time limits enabled"
msgstr "Uživatel ‘%s’ nemá nastaveny časové limity"

#: pam/pam_malcontent.c:151 pam/pam_malcontent.c:172
#, c-format
msgid "Error getting session limits for user ‘%s’: %s"
msgstr "Chyba při získávání limitů sezení pro uživatele ‘%s’: %s"

#: pam/pam_malcontent.c:182
#, c-format
msgid "User ‘%s’ has no time remaining"
msgstr "Uživatel ‘%s’ nemá žádný zbývající čas"

#: pam/pam_malcontent.c:200
#, c-format
msgid "Error setting time limit on login session: %s"
msgstr "Chyba při nastavení časového limitu na přihlašovací sezení: %s"

#~ msgid "All Ages"
#~ msgstr "libovolný věk"

#, c-format
#~ msgid "%s (%s)"
#~ msgstr "%s (%s)"

#~ msgid "General"
#~ msgstr "Obecné"

#~ msgid "ALL"
#~ msgstr "VŠECHNY"

#~ msgid "Adults Only"
#~ msgstr "pouze dospělé"

#~ msgid "Mature"
#~ msgstr "plnoleté"

#~ msgid "Teen"
#~ msgstr "mládež"

#~ msgid "Everyone 10+"
#~ msgstr "všechny 10+"

#~ msgid "Everyone"
#~ msgstr "všechny"

#~ msgid "Early Childhood"
#~ msgstr "malé děti"

#~ msgid "No cartoon violence"
#~ msgstr "Žádné kreslené násilí"

#~ msgid "Cartoon characters in unsafe situations"
#~ msgstr "Kreslené postavy v nebezpečných situacích"

#~ msgid "Cartoon characters in aggressive conflict"
#~ msgstr "Kreslené postavy v agresivních střetech"

#~ msgid "Graphic violence involving cartoon characters"
#~ msgstr "Grafické vyobrazení násilí páchaného na kreslených postavách"

#~ msgid "No fantasy violence"
#~ msgstr "Žádné fantazie o násilí"

#~ msgid "Characters in unsafe situations easily distinguishable from reality"
#~ msgstr "Osoby v nebezpečných situacích snadno odlišitelných od skutečnosti"

#~ msgid ""
#~ "Characters in aggressive conflict easily distinguishable from reality"
#~ msgstr "Osoby v agresivních střetech snadno odlišitelných od skutečnosti"

#~ msgid "Graphic violence easily distinguishable from reality"
#~ msgstr "Grafické násilí snadno odlišitelné od reality"

#~ msgid "No realistic violence"
#~ msgstr "Žádné realistické násilí"

#~ msgid "Mildly realistic characters in unsafe situations"
#~ msgstr "Mírně realistické osoby v nebezpečných situacích"

#~ msgid "Depictions of realistic characters in aggressive conflict"
#~ msgstr "Vyobrazení skutečných osob v agresivních konfliktech"

#~ msgid "Graphic violence involving realistic characters"
#~ msgstr "Grafické násilí páchané na skutečných osobách"

#~ msgid "No bloodshed"
#~ msgstr "Žádné zabíjení"

#~ msgid "Unrealistic bloodshed"
#~ msgstr "Nerealistické zabíjení"

#~ msgid "Realistic bloodshed"
#~ msgstr "Realistické zabíjení"

#~ msgid "Depictions of bloodshed and the mutilation of body parts"
#~ msgstr "Vyobrazení krveprolití a mrzačení lidí"

#~ msgid "No sexual violence"
#~ msgstr "Žádné sexuální násilí"

#~ msgid "Rape or other violent sexual behavior"
#~ msgstr "Znásilnění nebo jiné násilné sexuální chování"

#~ msgid "No references to alcohol"
#~ msgstr "Žádné zmínky o alkoholu"

#~ msgid "References to alcoholic beverages"
#~ msgstr "Zmínky o alkoholických nápojích"

#~ msgid "Use of alcoholic beverages"
#~ msgstr "Požívání alkoholických nápojů"

#~ msgid "No references to illicit drugs"
#~ msgstr "Žádné zmínky o zakázaných drogách"

#~ msgid "References to illicit drugs"
#~ msgstr "Zmínky o zakázaných drogách"

#~ msgid "Use of illicit drugs"
#~ msgstr "Používání zakázaných drog"

#~ msgid "References to tobacco products"
#~ msgstr "Zmínky o tabákových výrobcích"

#~ msgid "Use of tobacco products"
#~ msgstr "Používání tabákových výrobků"

#~ msgid "No nudity of any sort"
#~ msgstr "Žádná nahota jakéhokoliv typu"

#~ msgid "Brief artistic nudity"
#~ msgstr "Občasná umělecká nahota"

#~ msgid "Prolonged nudity"
#~ msgstr "Dlouhotrvající nahota"

#~ msgid "No references or depictions of sexual nature"
#~ msgstr "Žádné zmínky o sexu nebo jeho vyobrazení"

#~ msgid "Provocative references or depictions"
#~ msgstr "Provokativní zmínky nebo vyobrazení"

#~ msgid "Sexual references or depictions"
#~ msgstr "Zmínky o sexu nebo jeho vyobrazení"

#~ msgid "Graphic sexual behavior"
#~ msgstr "Grafické znázornění sexuálního chování"

#~ msgid "No profanity of any kind"
#~ msgstr "Žádné vulgární výrazy jakéhokoliv typu"

#~ msgid "Mild or infrequent use of profanity"
#~ msgstr "Lehké nebo občasné používání vulgárních výrazů"

#~ msgid "Moderate use of profanity"
#~ msgstr "Střídmé používání vulgárních výrazů"

#~ msgid "Strong or frequent use of profanity"
#~ msgstr "Hrubé nebo časté používání vulgárních výrazů"

#~ msgid "No inappropriate humor"
#~ msgstr "Žádný nevhodný humor"

#~ msgid "Slapstick humor"
#~ msgstr "Bláznivý humor"

#~ msgid "Vulgar or bathroom humor"
#~ msgstr "Vulgární nebo postelový humor"

#~ msgid "Mature or sexual humor"
#~ msgstr "Humor o sexu a pro dospělé"

#~ msgid "No discriminatory language of any kind"
#~ msgstr "Žádné diskriminační řeči jakéhokoliv druhu"

#~ msgid "Negativity towards a specific group of people"
#~ msgstr "Špatný pohled na konkrétní skupinu lidí"

#~ msgid "Discrimination designed to cause emotional harm"
#~ msgstr "Diskriminace cílící na způsobení citové újmy"

#~ msgid "Explicit discrimination based on gender, sexuality, race or religion"
#~ msgstr ""
#~ "Výslovná diskriminace založená na pohlaví, sexuální orientaci, rase nebo "
#~ "náboženství"

#~ msgid "No advertising of any kind"
#~ msgstr "Žádná reklama jakéhokoliv druhu"

#~ msgid "Product placement"
#~ msgstr "Vyobrazení produktů"

#~ msgid "Explicit references to specific brands or trademarked products"
#~ msgstr "Výslovné odkazy na konkrétní výrobce nebo značky výrobků"

#~ msgid "Users are encouraged to purchase specific real-world items"
#~ msgstr "Uživatelé jsou vybízení k nákupu konkrétního skutečného zboží"

#~ msgid "No gambling of any kind"
#~ msgstr "Žádné hraní jakéhokoliv druhu"

#~ msgid "Gambling on random events using tokens or credits"
#~ msgstr "Příležitostné hraní s žetony nebo kreditem"

#~ msgid "Gambling using “play” money"
#~ msgstr "Hraní o fiktivní peníze"

#~ msgid "Gambling using real money"
#~ msgstr "Hraní o skutečné peníze"

#~ msgid "No ability to spend money"
#~ msgstr "Žádná možnost prohrát skutečné peníze"

#~ msgid "Users are encouraged to donate real money"
#~ msgstr "Uživatelé jsou vybízení k přispění skutečnými penězi"

#~ msgid "Ability to spend real money in-game"
#~ msgstr "Možnost prohrát skutečné peníze"

#~ msgid "No way to chat with other users"
#~ msgstr "Žádný způsob, jak komunikovat s ostatními uživateli"

#~ msgid "Moderated chat functionality between users"
#~ msgstr "Moderovaná textová komunikace mezi uživateli"

#~ msgid "Uncontrolled chat functionality between users"
#~ msgstr "Neřízená textová komunikace mezi uživateli"

#~ msgid "No way to talk with other users"
#~ msgstr "Žádný způsob, jako hovořit s ostatními uživateli"

#~ msgid "Uncontrolled audio or video chat functionality between users"
#~ msgstr "Neřízená zvuková nebo obrazová komunikace mezi uživateli"

#~ msgid "No sharing of social network usernames or email addresses"
#~ msgstr ""
#~ "Žádné sdílení uživatelských jmen ze sociálních sítí a e-mailových adres"

#~ msgid "Sharing social network usernames or email addresses"
#~ msgstr "Sdílení uživatelských jmen ze sociálních sítí a e-mailových adres"

#~ msgid "No sharing of user information with 3rd parties"
#~ msgstr "Žádné sdílení informací o uživateli se třetími stranami"

#~ msgid "Checking for the latest application version"
#~ msgstr "Zjišťuje se nejnovější verze aplikace"

#~ msgid "Sharing diagnostic data that does not let others identify the user"
#~ msgstr ""
#~ "Sdílení diagnostických dat, dle kterých se nedá identifikovat uživatel"

#~ msgid "Sharing information that lets others identify the user"
#~ msgstr "Sdílení informací, dle kterých se dá identifikovat uživatel"

#~ msgid "No sharing of physical location to other users"
#~ msgstr "Žádné sdílení fyzické polohy s ostatními uživateli"

#~ msgid "Sharing physical location to other users"
#~ msgstr "Sdílení fyzické polohy s ostatními uživateli"

#~ msgid "No references to homosexuality"
#~ msgstr "Žádné zmínky o homosexualitě"

#~ msgid "Indirect references to homosexuality"
#~ msgstr "Nepřímé zmínky o homosexualitě"

#~ msgid "Kissing between people of the same gender"
#~ msgstr "Polibky dvou lidí stejného pohlaví"

#~ msgid "Graphic sexual behavior between people of the same gender"
#~ msgstr "Grafické vyobrazení sexuálního chování mezi lidmi stejného pohlaví"

#~ msgid "No references to prostitution"
#~ msgstr "Žádné zmínky o prostituci"

#~ msgid "Indirect references to prostitution"
#~ msgstr "Nepřímé zmínky o prostituci"

#~ msgid "Direct references to prostitution"
#~ msgstr "Přímé zmínky o prostituci"

#~ msgid "Graphic depictions of the act of prostitution"
#~ msgstr "Grafické vyobrazení aktu prostituce"

#~ msgid "No references to adultery"
#~ msgstr "Žádné zmínky o cizoložství"

#~ msgid "Indirect references to adultery"
#~ msgstr "Nepřímé zmínky o cizoložství"

#~ msgid "Direct references to adultery"
#~ msgstr "Přímé zmínky o cizoložství"

#~ msgid "Graphic depictions of the act of adultery"
#~ msgstr "Grafické vyobrazení aktu cizoložství"

#~ msgid "No sexualized characters"
#~ msgstr "Žádné sexualizované postavy"

#~ msgid "Scantily clad human characters"
#~ msgstr "Skrovně oděné lidské postavy"

#~ msgid "Overtly sexualized human characters"
#~ msgstr "Otevřeně sexualizované lidské postavy"

#~ msgid "No references to desecration"
#~ msgstr "Žádné zmínky o znesvěcení"

#~ msgid "Depictions of modern-day human desecration"
#~ msgstr "Vyobrazení znesvěcení lidmi v moderní době"

#~ msgid "Graphic depictions of modern-day desecration"
#~ msgstr "Grafické vyobrazení znesvěcení v moderní době"

#~ msgid "No visible dead human remains"
#~ msgstr "Žádné viditelné pozůstatky lidských mrtvol"

#~ msgid "Visible dead human remains"
#~ msgstr "Viditelné pozůstatky lidských mrtvol"

#~ msgid "Dead human remains that are exposed to the elements"
#~ msgstr "Ostatky lidských mrtvol zabrané do detailů"

#~ msgid "Graphic depictions of desecration of human bodies"
#~ msgstr "Grafické vyobrazení znesvěcení lidských těl"

#~ msgid "No references to slavery"
#~ msgstr "Žádné zmínky o otroctví"

#~ msgid "Depictions of modern-day slavery"
#~ msgstr "Vyobrazení otroctví v moderní době"

#~ msgid "Graphic depictions of modern-day slavery"
#~ msgstr "Grafické vyobrazení otroctví v moderní době"
